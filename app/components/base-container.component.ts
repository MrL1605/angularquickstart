/**
 * Created by Lalit Umbarkar
 * Date: 4/8/17
 * Project: generated-ang
 */

import {Component, OnInit} from "@angular/core";
import {BaseService} from "../services/base.service";

@Component({
    selector: "base-container",
    template: require("../templates/base-container.template.html"),
})
export class BaseContainerComponent implements OnInit {

    constructor(private service: BaseService) {
    }

    dateToChange(e:any){
        console.log(e);
    }

    ngOnInit() {

    }

    testLink(){
        this.service.testCx6().subscribe((info) => {
            console.log(info);
        }, (err) => {
            console.log(err);
        })
    }

}

