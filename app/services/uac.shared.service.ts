/**
 * Created By : Lalit
 * Created On : 8/10/17
 * Organisation: CustomerXPs Software Private Ltd.
 */

export class UACStatic {
    public static debugUrlPrefix: string = "http://localhost:9090/uac/1.0";
    public static uacBaseUrl: string = "http://lalit.customerxps.com:5000/clari5-cc/rest/1.0";
    public static urlPrefix: string = UACStatic.uacBaseUrl;
}
