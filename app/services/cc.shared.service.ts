/**
 * Created By : Lalit
 * Created On : 8/10/17
 * Organisation: CustomerXPs Software Private Ltd.
 */

import {Observable} from "rxjs";
import {BaseRequestOptions} from "@angular/http";

declare var loggedInUserId : string;

export class SharedService {

    public static authenticationType : string = "";

    public static loggedInTenant : string = "GLOBAL";

    public static loggedInUserId : string = "";

    public static loggedInSiteId : string = "DEFAULT";

    public static isProcessRunning: boolean = false;

    public static isTopologyUploaded: boolean = false;

    public static canProceedToDeploy: boolean = false;

    public static isDeadAppsFound : boolean = false;

    public static isLicesePresent: boolean = false;

    public static isLoggedIn: boolean = false;

    public static currentSelectedVersion: string;

    public static baseUrl: string = "http://lalit.customerxps.com:5000/clari5-cc/rest/1.0";

    //loading ajaxLoader.gif
    public static loadingGifPath: string = "/cdn/platform/images/ajaxLoader.gif";

    public static getFormattedError(errorText: string): string {
        let htmlHead = errorText.split("<style")[0];
        let htmlBody = errorText.split("</style>")[1];
        if (htmlBody === undefined) {
            htmlBody = "";
        } else {
            htmlBody = htmlBody.replace("h1", "h3").replace("h1", "h3");
        }
        return htmlHead + htmlBody;
    }


    public static extractText(response: any){
        try{
            return response.text();
        }catch(e){
            if (!response.ok && response.status === 401){
                window.location.href = window.location.origin+"/clari5-cc";
            }
        }
    }

    public static extractJson(response: any){
        try{
            return response.json();
        }catch(e){
            // Try redirect code
            if (!response.ok && response.status === 401){
                window.location.href = window.location.origin+"/clari5-cc";
            }
        }
    }

    public static handleError(error: any){
        if (error.status === 401){
            window.location.href = window.location.origin+"/clari5-cc";
        }
        return Observable.throw(error);
    }

}


/**
 * This class must be used by every service while sending custom options to
 * ensure that Finger Print Data is sent to backend for validation,
 * otherwise session will be assumed invalid, and user will be logged Out.
 *
 * Also using options during http calls is optional, as this class is used by default.
 * */
export class CustomRequestOptions extends BaseRequestOptions {
    constructor() {
        super();
        // Headers Used for Device Finger Printing
        let res = window.screen.width + 'x' + window.screen.height;
        let os_core = navigator.hardwareConcurrency;
        let platform = navigator.platform;
        this.headers.append("Content-type", "application/json");
        this.headers.append("resolution", res);
        this.headers.append("timezone", "" + new Date().getTimezoneOffset());
        this.headers.append("cpu-core", "" + os_core);
        this.headers.append("platform", platform);
    }

    static getHeaders() : any{
        let headers = [];
        let res = window.screen.width + 'x' + window.screen.height;
        let os_core = navigator.hardwareConcurrency;
        let platform = navigator.platform;
        headers.push({name : "resolution", value: res});
        headers.push({name : "timezone", value: "" + new Date().getTimezoneOffset()});
        headers.push({name : "cpu-core", value: "" + os_core});
        headers.push({name : "platform", value: platform});
        return headers;
    }

    static getHeadersObject(): any {
        let headers = new Headers();
        let res = window.screen.width + 'x' + window.screen.height;
        let os_core = navigator.hardwareConcurrency;
        let platform = navigator.platform;
        headers.append("Content-type", "application/json");
        headers.append("resolution", res);
        headers.append("timezone", "" + new Date().getTimezoneOffset());
        headers.append("cpu-core", "" + os_core);
        headers.append("platform", platform);
        return headers;
    }

}
