import {NgModule} from "@angular/core";
import {AppRouter} from "./app.router";
import {BaseContainerComponent} from "./components/base-container.component";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {BaseService} from "./services/base.service";
// import {RoleComponent} from "./gen/role/component";
import {SharedService} from "./services/cc.shared.service";
// import {RoleService} from "./gen/role/service";
import {Ng2Bs3ModalModule} from "ng2-bs3-modal/ng2-bs3-modal";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";



@NgModule({
    declarations: [
        BaseContainerComponent,
        /*RoleComponent*/
    ],
    imports: [
        BrowserAnimationsModule, BrowserModule, FormsModule, HttpModule, AppRouter, Ng2Bs3ModalModule
    ],
    providers: [
        BaseService, SharedService, /*RoleService*/
    ],
    bootstrap: [
        BaseContainerComponent
    ]
})
export class AppModule {
}


